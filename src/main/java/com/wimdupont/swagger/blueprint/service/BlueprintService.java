package com.wimdupont.swagger.blueprint.service;

import com.wimdupont.swagger.blueprint.controller.dto.BlueprintDto;
import com.wimdupont.swagger.blueprint.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class BlueprintService {

    private final Set<BlueprintDto> blueprintDtoSet = new HashSet<>();

    public BlueprintService() {
        blueprintDtoSet.add(BlueprintDto.Builder.newBuilder()
                .id(UUID.randomUUID())
                .name("blueprint-one")
                .number(420)
                .build());
        blueprintDtoSet.add(BlueprintDto.Builder.newBuilder()
                .id(UUID.randomUUID())
                .name("blueprint-two")
                .number(666)
                .build());
    }

    public Set<BlueprintDto> getBlueprints() {
        return blueprintDtoSet;
    }

    public BlueprintDto getBlueprint(UUID id) {
        return getBlueprints().stream()
                .filter(f -> f.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new EntityNotFoundException(BlueprintDto.class, id.toString()));
    }

    public BlueprintDto createBlueprint(BlueprintDto blueprintDto) {
        blueprintDto.setId(UUID.randomUUID());
        blueprintDtoSet.add(blueprintDto);
        return blueprintDto;
    }

    public BlueprintDto updateBlueprint(UUID id, BlueprintDto blueprintDto) {
        var existing = getBlueprint(id);
        existing.setName(blueprintDto.getName());
        existing.setNumber(blueprintDto.getNumber());
        return existing;
    }

    public void deleteBlueprint(UUID id) {
        getBlueprints().remove(getBlueprint(id));
    }
}
