package com.wimdupont.swagger.blueprint.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityNotFoundException extends RuntimeException {

    public <T> EntityNotFoundException(Class<T> clazz, String id) {
        super(String.format("Entity (%s) not found for id: %s", clazz.getSimpleName(), id));
    }
}
