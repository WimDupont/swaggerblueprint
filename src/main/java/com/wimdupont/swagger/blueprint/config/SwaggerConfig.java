package com.wimdupont.swagger.blueprint.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

@Configuration
public class SwaggerConfig {
    private static final String DOCUMENTATION_FILE = "documentation.md";
    private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerConfig.class);

    @Bean
    public OpenAPI getOpenAPI() {
        return new OpenAPI()
                .info(getInfo());
    }

    private Info getInfo() {
        Info info = new Info().title("Blueprint")
                .version("v1");

        URL url = getClass().getClassLoader().getResource(DOCUMENTATION_FILE);
        if (url != null) {
            try {
                info.description(new String(Files.readAllBytes(new File(url.getFile()).toPath())));
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return info;
    }

    @Bean
    public GroupedOpenApi overviewGroupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("overview")
                .pathsToMatch("/**")
                .build();
    }

}
