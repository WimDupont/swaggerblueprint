package com.wimdupont.swagger.blueprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class SwaggerBlueprintApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerBlueprintApplication.class, args);
    }
}
