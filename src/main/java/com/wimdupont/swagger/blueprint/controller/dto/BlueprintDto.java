package com.wimdupont.swagger.blueprint.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.wimdupont.swagger.blueprint.controller.dto.validation.ReadGroup;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

@JsonDeserialize(builder = BlueprintDto.Builder.class)
public class BlueprintDto {

    @NotNull(groups = ReadGroup.class)
    @Schema(accessMode = AccessMode.READ_ONLY,
            description = "Unique identifier of the blueprint",
            example = "030e5b9d-b4ed-4900-af24-cb2eac570056")
    private UUID id;

    @NotNull
    @Schema(description = "Name of the blueprint",
            example = "blueprint-name")
    private String name;

    @Min(10)
    @Schema(description = "Number of the blueprint",
            example = "42")
    private Integer number;

    private BlueprintDto(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setNumber(builder.number);
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private @NotNull UUID id;
        private @NotNull String name;
        private @Min(10) Integer number;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(@NotNull UUID val) {
            id = val;
            return this;
        }

        public Builder name(@NotNull String val) {
            name = val;
            return this;
        }

        public Builder number(@Min(10) Integer val) {
            number = val;
            return this;
        }

        public BlueprintDto build() {
            return new BlueprintDto(this);
        }
    }
}
