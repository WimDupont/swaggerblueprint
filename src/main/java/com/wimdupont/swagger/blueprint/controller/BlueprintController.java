package com.wimdupont.swagger.blueprint.controller;

import com.wimdupont.swagger.blueprint.controller.dto.BlueprintDto;
import com.wimdupont.swagger.blueprint.service.BlueprintService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/blueprints")
public class BlueprintController {

    private final BlueprintService blueprintService;

    public BlueprintController(BlueprintService blueprintService) {
        this.blueprintService = blueprintService;
    }

    @GetMapping
    @Operation(summary = "Retrieve blueprints", description = "This will list all the unique blueprints")
    public Set<BlueprintDto> getBlueprints() {
        return blueprintService.getBlueprints();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Retrieve blueprint", description = "This will retrieve the existing blueprint")
    public BlueprintDto getBlueprint(@PathVariable UUID id) {
        return blueprintService.getBlueprint(id);
    }

    @PostMapping
    @Operation(summary = "Create blueprint", description = "This will create a new blueprint")
    public BlueprintDto createBlueprint(@RequestBody @Valid BlueprintDto blueprintDto) {
        return blueprintService.createBlueprint(blueprintDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update blueprint", description = "This will update the existing blueprint")
    public BlueprintDto updateBlueprint(@PathVariable UUID id, @RequestBody @Valid BlueprintDto blueprintDto) {
        return blueprintService.updateBlueprint(id, blueprintDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete blueprint", description = "This will delete the existing blueprint")
    public void deleteBlueprint(@PathVariable UUID id) {
        blueprintService.deleteBlueprint(id);
    }

}
