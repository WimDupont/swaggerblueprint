package com.wimdupont.swagger.blueprint.controller;

import com.wimdupont.swagger.blueprint.service.BlueprintService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BlueprintController.class)
@AutoConfigureMockMvc
class BlueprintControllerTest extends BaseMvcTest {

    @MockBean
    private BlueprintService blueprintService;

    @Test
    void getBlueprints() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/blueprints")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpectAll(status().isOk());

        verify(blueprintService).getBlueprints();
    }

}
