package com.wimdupont.swagger.blueprint.service;

import com.wimdupont.swagger.blueprint.controller.dto.BlueprintDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BlueprintServiceTest {

    private BlueprintService blueprintService;

    @BeforeEach
    void setup() {
        blueprintService = new BlueprintService();
    }

    @Test
    void getBlueprints() {
        Set<BlueprintDto> result = blueprintService.getBlueprints();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

}
