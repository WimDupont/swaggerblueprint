#!/bin/sh

base_url="http://localhost:8080/blueprints"

with_error=0

while getopts "e" arg; do
	case $arg in
		e) with_error=1;;
		*);;
    	esac
done

shift $((OPTIND-1))

id=$1
body_name=
body_number=

get_id () {
	if [ -z "${id}" ]; then
		echo "For what UUID?"
		read -r id
	fi
}

get_url () {
	base_url="${base_url}$1"
	if [ "${with_error}" -eq 1 ]; then
		base_url="${base_url}?errors=true"
	fi
	echo "${base_url}"
}

get_body_name () {
	echo "Request name?"
	read -r body_name
}

get_body_number () {
	echo "Request number?"
	read -r body_number
}

request=

while [[ "${method}" != @(q|quit) ]]
do
	echo "Request method? (GET/POST/PUT/DELETE) - 'q' or 'quit' to exit script."
	read -r method

	case ${method^^} in

		GET)
			get_id
			if [ -z "${id}" ]; then
				request=$(curl -X GET -sw '%{http_code}' $(get_url) | jq)
			else
				request=$(curl -X GET -sw '%{http_code}' $(get_url "/${id}") | jq)
			fi
			;;

		POST)
			get_body_name
			get_body_number
			request=$(curl -X POST -sw '%{http_code}' $(get_url) -H 'Content-Type: application/json' -d "{\"name\":\"${body_name}\",\"number\":\"${body_number}\"}" | jq)
			;;

		PUT)
			get_id
			get_body_name
			get_body_number
			request=$(curl -X PUT -sw '%{http_code}' $(get_url "/${id}") -H 'Content-Type: application/json' -d "{\"name\":\"${body_name}\",\"number\":\"${body_number}\"}"| jq)
			;;

		Q|QUIT)
			exit 0
			;;

		DELETE)
			get_id
			request=$(curl -k -X 'DELETE' -sw '%{http_code}' $(get_url "/${id}") | jq)
			;;

		*)
			echo "Unknown http method."
			;;
	esac

	echo ${method^^}
	echo "${request}"
	retrieved_id=$(echo "${request}" | jq "try .[0].id catch \"JQ_ERROR\"" | head -n 1 | tr -d \")

	if [ "${retrieved_id}" == "JQ_ERROR" ]; then
		retrieved_id=$(echo "${request}" | jq "try .id catch \"JQ_ERROR\"" | head -n 1 | tr -d \")
	fi

	echo "${retrieved_id}" | xclip

	id=
	body_name=
	body_number=
done
